<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Shop\Customers\Customer;
class Customrule implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $cust = Customer::where('datee' , $value)->get();
        if($cust->count() >= 15){
            return false;
        }else{
            return true;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Maximum 15 .';
    }
}

?>