<?php

namespace App\Shop\Blocks;

use Illuminate\Database\Eloquent\Model;
use DB;
class Block extends Model
{
   public static function createBlock($data){
	  return DB::table('blocks')->insert($data);
   }
   public static function updateBlock($data,$id){
	  return DB::table('blocks')->where('block_id', $id)->update($data);
   }
   public static function deleteBlock($id){
	  return DB::table('blocks')->where('block_id', $id)->update($data);
   }
}
