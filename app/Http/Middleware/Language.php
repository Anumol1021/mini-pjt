<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Cookie;
use Crypt;
class Language
{
	/**
     * @var ProductRepositoryInterface
     */
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next )
    {
		
		if($request->has('_lang')){
			//echo $request->input('_lang');exit;
			if(request()->segment(1) !='admin')
				app()->setLocale($request->input('_lang'));
			setcookie('_lang', $request->input('_lang'), time() + (86400 * 30), "/"); // 86400 = 1 day
			view()->share('__lang', $request->input('_lang'));
		}else{
			if($this->hasCookie('_lang')) {
				if(request()->segment(1) !='admin')
					app()->setLocale($_COOKIE['_lang']);
				view()->share('__lang', $_COOKIE['_lang']);
			}else{
				view()->share('__lang', 'en');
			}
		}
        return $next($request);
    }
	protected function makeMyCookie($val)
	{
		return Cookie::queue(Cookie::make('_lang', $val, 129600));
	}

	protected function hasCookie($cookie_name)
	{
		 $cookie_exist = Cookie::get($cookie_name);
		 return ($cookie_exist) ? true : false;
	}
	protected function getCookie($cookie_name)
	{
		 echo Crypt::decrypt(Cookie::get($cookie_name));
		 return dd(Cookie::get($cookie_name));
	}
}
