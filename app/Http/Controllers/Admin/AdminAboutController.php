<?php
/*
Project Name: IonicEcommerce
Project URI: http://ionicecommerce.com
Author: VectorCoder Team
Author URI: http://vectorcoder.com/
Version: 2.1
*/
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class AdminAboutController extends Controller
{
	//listingTaxClass
	public function listingAbout(Request $request){
		$title = array('pageTitle' => Lang::get("labels.About"));		
		
		$result = array();
		$message = array();
			
		$about = DB::table('about')->paginate(20);
		
		$result['message'] = $message;
		$result['about'] = $about;
		
		return view("admin.listingAbout", $title)->with('result', $result);
	}
	//addTaxClass
	public function addAbout(Request $request){
		$title = array('pageTitle' => Lang::get("labels.About"));
		$result = array();
		$message = array();
		$result['message'] = $message;
		return view("admin.addAbout", $title)->with('result', $result);
	}

	public function addNewAbout(Request $request){
		$title = array('pageTitle' => Lang::get("labels.About"));
		
		$id = DB::table('about')->insertGetId([
				'name'   =>   $request->name,
				'description'  => $request->description,
		]);
		$message = "Details has been added successfully!";
		return redirect()->back()->withErrors([$message]);
	}
	//editTaxClass
	public function editAbout(Request $request){		
		$title = array('pageTitle' => Lang::get("labels.EditDetails"));
		$result = array();		
		$result['message'] = array();
		
		$about = DB::table('about')->where('id', $request->id)->get();
		$result['about'] = $about;
		return view("admin.editAbout",$title)->with('result', $result);
	}
	//updateTaxClass
	public function updateAbout(Request $request){
			$title = array('pageTitle' => Lang::get("labels.EditDetails"));

			$message = "Details has been updated successfully!";
			$companyUpdate = DB::table('about')->where('id', $request->id)->update([
						'name'  		 =>   $request->name,
						'description'  => $request->description,
			]);
			return redirect()->back()->withErrors([$message ]);
	}
	//deleteCountry
	public function deleteAbout(Request $request){
		DB::table('about')->where('id', $request->id)->delete();
		return redirect()->back()->withErrors("Details Deleted");
	}
	
}                                                                                     
