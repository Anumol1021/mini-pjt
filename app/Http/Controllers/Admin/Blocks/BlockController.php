<?php

namespace App\Http\Controllers\Admin\Blocks;

use App\Http\Controllers\Controller;
use App\Shop\Blocks\Block;
//validator is builtin class in laravel
use Validator;

use App;
use Lang;

use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 
use Illuminate\Http\Request;
//use Illuminate\Routing\Controller;


class BlockController extends Controller
{
    
   
	public function index(Request $request){
		$title = array('pageTitle' => "Blocks");		
		
		$result = array();
		$message = array();
			
		$blocks = DB::table('blocks')->paginate(20);
		
		$result['message'] = $message;
		$result['blocks']  = $blocks;
		
		return view("admin.blocks.list", $title)->with('result', $result);
	}

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.blocks.create');
    }

    /**
     * @param CreateBrandRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        Block::createBlock($request->except('_token'));
        return redirect()->route('admin.block.index')->with('message', 'Create brand successful!');
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
		return view('admin.blocks.edit', ['block' => Block::where('block_id', $id)->first()]);
    }

    /**
     * @param UpdateBrandRequest $request
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \App\Shop\Brands\Exceptions\UpdateBrandErrorException
     */
    public function update(Request $request, $id)
    {
        Block::updateBlock($request->except('_token'),$id);
        return redirect()->route('admin.block.edit', $id)->with('message', 'Update successful!');
    }

    /**
     * @param $id
     */
    public function deleteblock($id)
    {
		Block::deleteBlock($id);
        return redirect()->route('admin.brands.index')->with('message', 'Delete successful!');
    }
}
