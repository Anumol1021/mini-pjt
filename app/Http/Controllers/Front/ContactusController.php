<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Shop\Subscribers\Subscribers;
//validator is builtin class in laravel
use Validator;
use App;
use Lang;
use DB;
//for password encryption or hash protected
use Hash;
use App\Administrator;

//for authenitcate login data
use Auth;
use Mail;
use File;
use Illuminate\Support\Facades\Redirect;

//use Illuminate\Foundation\Auth\ThrottlesLogins;
//use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

//for requesting a value 

class ContactusController extends Controller
{
	public function index(){
		$view = 'front.contactus';
				
		return view($view, []);
	}
	public function send(Request $request){
		
		$data = $request->all();
		Mail::send('emails.contactus', [
            'contact' => $data,
        ], function ($message){
			$message
			  ->from('noreply@whyteapps.com', 'Al saqr')
			  ->to('chinjuraju1204@gmail.com', "Chinju")
			  ->subject('Contact From ');
		});
		
		return redirect()->route('contactus.index')
            ->with('message', 'Thank you for contacting us.');
		//Unable to submit your request. Please, try again later
		//echo "sssssssssssssssssssssssss";exit;
		//return view('front.page', []);
	}
	
}
