<?php

namespace App\Repositories\Eloquent;

use App\subscribers;
use App\Repositories\Contracts\subscribersRepository;

use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentsubscribersRepository extends AbstractRepository implements subscribersRepository
{
    public function entity()
    {
        return subscribers::class;
    }
}
