<?php $__env->startSection('content'); ?>

<section class="abt-tp">
	
	<div class="container">
		<div class="col-md-12">
			<h3>Career</h3>
			<ul class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
  <li class="breadcrumb-item active">Career</li>
</ul>
		</div>
	</div>
	
</section>



<section class="crr clearfix">
	<div class="container">
		<div class="col-md-12">
			<h4>Coming Soon </h4>
			<ul class="gridder">
              <!--  <li class="gridder-list" data-griddercontent="#gridder-content-1" data-aos="fade-up">
                   <figure><figcaption>DEPUTY FINANCE MANAGER  <br> <span>Doha - Qatar</span> <h5>Apply Now</h5></figcaption> <div class="scroll-down"><div class="scroll-down__line"></div></div></figure>
                </li>
                <li class="gridder-list" data-griddercontent="#gridder-content-2" data-aos="fade-up" data-aos-duration="1200">
                   <figure><figcaption>DEPUTY FINANCE MANAGER  <br> <span>Doha - Qatar</span> <h5>Apply Now</h5></figcaption> <div class="scroll-down"><div class="scroll-down__line"></div></div></figure>
                </li>
                <li class="gridder-list" data-griddercontent="#gridder-content-3" data-aos="fade-up" data-aos-duration="1600">
                   <figure><figcaption>DEPUTY FINANCE MANAGER  <br> <span>Doha - Qatar</span> <h5>Apply Now</h5></figcaption> <div class="scroll-down"><div class="scroll-down__line"></div></div></figure>
                </li>
                <li class="gridder-list" data-griddercontent="#gridder-content-4" data-aos="fade-up" data-aos-duration="2000">
                   <figure><figcaption>DEPUTY FINANCE MANAGER  <br> <span>Doha - Qatar</span> <h5>Apply Now</h5></figcaption> <div class="scroll-down"><div class="scroll-down__line"></div></div></figure>
                </li>
                <li class="gridder-list" data-griddercontent="#gridder-content-5" data-aos="fade-up">
                   <figure><figcaption>DEPUTY FINANCE MANAGER  <br> <span>Doha - Qatar</span> <h5>Apply Now</h5></figcaption> <div class="scroll-down"><div class="scroll-down__line"></div></div></figure>
                </li>-->
            </ul>
		</div>
		
		
	<!--	<div id="gridder-content-1" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
        
        
        
       <!-- <div id="gridder-content-1" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
        
        
      <!--  <div id="gridder-content-2" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
        
        
      <!--  <div id="gridder-content-3" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
        
        
       <!-- <div id="gridder-content-4" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
        
        
        
       <!-- <div id="gridder-content-5" class="gridder-content">
           <div class="container">
           <div class="col-md-12">
           	<div class="row">
           		<div class="col-md-12">
           			<div class="row">
           			    <div class="col-md-7 crr-des">
           			     <h5>Job Summary/Overview</h5>
						  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
         			    <h5>Duties and Essential Job Functions</h5>
         			    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>
         			    
          			    </div>
           				<div class="col-md-5">
						   <form>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Name"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Email"><div class="form_line"></div></div>
						   	<div class="form-group"><input type="text" class="form-control" placeholder="Phone"><div class="form_line"></div></div>
						   	<div class="form-group">
								<input class="choose-file" type="file" id="choose-file" required>
								<label for="choose-file" class="upload-file"></label>
							</div>
						   	<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea><div class="form_line"></div></div>
						   	<button class="con-btn">SUBMIT</button>
						   </form>
          			    </div>
           			</div>
           			
           		</div>
           	</div>
           </div>
          </div>          
        </div>-->
		
		
	</div>
</section>
 <?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>