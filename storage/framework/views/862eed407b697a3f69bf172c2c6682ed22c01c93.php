<?php $__env->startSection('content'); ?>
<section class="rw">
	<div class="container">
		<h3><?php echo strip_tags($comdetails->company_name); ?></h3>
		<h5><?php echo strip_tags($comdetails->short_desc); ?></h5>
	</div>
	  <?php if($comdetails->banner_image): ?>
	<div class="pp-img "><img src="<?php echo asset($comdetails->banner_image); ?>"></div>
	  <?php else: ?>
	  <?php endif; ?>
</section>

<section class="rs">
	<div class="container">
		<div class="col-md-12">
			<h4 data-aos="fade-up"> <div class="scroll-down"><div class="scroll-down__line"></div></div></h4>
			<p data-aos="fade-up"><?php echo $comdetails->description; ?></p>
		</div>
	</div>
</section>

  <?php if($comdetails->product_details): ?>
<section class="rp">
	<div class="container">
		<div class="col-md-12">
			<h4 data-aos="fade-up">OUR PRODUCTS <div class="scroll-down"><div class="scroll-down__line"></div></div></h4>
			<div class="row">
				<?php echo $comdetails->product_details; ?>

			</div>
		</div>
	</div>
</section>
  <?php else: ?>
  <?php endif; ?>

  <?php if($comdetails->link): ?>
<section class="rp">
	<a href="<?php echo e($comdetails->link); ?>" target="_blank" class="link-web module"><?php echo e($comdetails->link); ?></a>
</section>
  <?php else: ?>
  <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.apphome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>