<div class="super_container">
<header class="header d-flex flex-row justify-content-end align-items-center trans_200">
    
    <!-- Logo -->
    <div class="logo mr-auto">
      <a href="<?php echo e(route('home')); ?>"><img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png"></a>
    </div>

    <!-- Navigation -->
    <nav class="main_nav justify-self-end text-right">
      <ul>
        <li><a href="<?php echo e(route('home')); ?>">Home</a></li>
        <li><a href="<?php echo e(route('aboutus')); ?>">About Us</a></li>
        <li><a href="<?php echo e(route('solution')); ?>">Solution</a></li>
        <li><a href="<?php echo e(route('services')); ?>">Services</a></li>
        <li><a href="<?php echo e(route('contactus.index')); ?>">Contact</a></li>
      </ul>
      
    </nav>

    <!-- Hamburger -->
    <div class="hamburger_container bez_1">
      <i class="fas fa-bars trans_200"></i>
    </div>
    
  </header>

  <!-- Menu -->

  <div class="menu_container">
    <div class="menu menu_mm text-right">
      <div class="menu_close"><i class="far fa-times-circle trans_200"></i></div>
      <ul class="menu_mm">
        <li class="menu_mm"><a href="<?php echo e(route('home')); ?>">Home</a></li>
        <li class="menu_mm"><a href="<?php echo e(route('aboutus')); ?>">About Us</a></li>
        <li class="menu_mm"><a href="<?php echo e(route('solution')); ?>">Solution</a></li>
        <li class="menu_mm"><a href="<?php echo e(route('services')); ?>">Services</a></li>
        <li class="menu_mm"><a href="<?php echo e(route('contactus.index')); ?>">Contact</a></li>
      </ul>
    </div>
  </div>
