<?php $__env->startSection('content'); ?>
<!-- Main content -->
    <section class="content">
    <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Service</h2>
            <div class="row">
              <div class="col-xs-12">
        
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Register Id</th>
                      <th>date</th>
					  <th>Name</th>
                      <th>Token Number</th>
                      <th>status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if(count($result['appointments'])>0): ?>
                    <?php $__currentLoopData = $result['appointments']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$appointments): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($appointments->id); ?></td>
                            <td><?php echo e($appointments->regid); ?></td>
                            <td><?php echo e($appointments->datee); ?></td>
							<td><?php echo e($appointments->name); ?></td>
                            <td><?php echo e($appointments->tokenno); ?></td>
                            <td><?php echo e($appointments->status); ?></td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editAppointment/<?php echo e($appointments->id); ?>" class="badge bg-light-blue">edit</a> 
                            
                           
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  <?php echo e($result['appointments']->links()); ?>

                </div>
              </div>
            </div>
          </div>
     </section>  



<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>