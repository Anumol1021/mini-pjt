<?php $__env->startSection('content'); ?>
<section class="abt-tp">
	<div class="container">
		<div class="col-md-12">
			<h3>Companies</h3>
			<ul class="breadcrumb">
			  <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
			  <li class="breadcrumb-item active">Companies</li>
			</ul>
		</div>
	</div>
</section>

<section class="osub clearfix">
	<div class="container clearfix">
		<div class="col-md-12 clearfix">
			  <?php if(!$company->isEmpty()): ?>
		      <?php $__currentLoopData = array_chunk($company->toArray(), 2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companies): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<ul class="clearfix">
				 <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $companys): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>  
				<li data-aos="fade-up"><a href="<?php echo e(route('comdetails',$companys->slug)); ?>">
					<div class="osub-txt"><img src="<?php echo asset($companys->logo); ?>">
					<h4><?php echo $companys->company_name; ?></h4>
					<p><?php echo e($companys->short_desc); ?></p>
					</div>
					<div class="osub-img"><div class="scroll-down"><div class="scroll-down__line"></div></div><img src="<?php echo asset($companys->small_image); ?>"></div>
				</a></li>
				 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
			  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              <?php endif; ?>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>