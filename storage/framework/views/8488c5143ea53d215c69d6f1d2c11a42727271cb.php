<?php $__env->startSection('content'); ?>
<!-- Main content -->
    <section class="content">
    <?php echo $__env->make('layouts.errors-and-messages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Banners</h2>
            <div class="row">
              <div class="col-xs-12">
			     <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="<?php echo e(route('admin.addBanner')); ?>"><i class="fa fa-plus"></i> Create Banner</a>
                 </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Title</th>
                      <th>Description</th>
                      <th>Image</th>
					    <th>Type</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php if(count($result['banners'])>0): ?>
                    <?php $__currentLoopData = $result['banners']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$banners): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                            <td><?php echo e($banners->banners_id); ?></td>
                            <td><?php echo e($banners->banners_title); ?></td>
							<td><?php echo e($banners->banners_desc); ?></td>
                            <td><img src="<?php echo e(asset('').'/'.$banners->banners_image); ?>" alt="" width=" 100px"></td>
							<td><?php echo e($banners->type); ?></td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editBanner/<?php echo e($banners->banners_id); ?>" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                            <a data-toggle="tooltip" data-placement="bottom" title="Delete" id="deleteBannerId" banners_id ="<?php echo e($banners->banners_id); ?>" class="badge bg-red"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </tr>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php else: ?>
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    <?php endif; ?>
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                	<?php echo e($result['banners']->links()); ?>

                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
	<div class="modal fade" id="deleteBannerModal" tabindex="-1" role="dialog" aria-labelledby="deleteBannerModalLabel">
	  <div class="modal-dialog" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title" id="deleteBannerModalLabel">Delete Banner</h4>
		  </div>
		  <?php echo Form::open(array('url' =>'admin/deleteBanner', 'name'=>'deleteBanner', 'id'=>'deleteBanner', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

				  <?php echo Form::hidden('action',  'delete', array('class'=>'form-control')); ?>

				  <?php echo Form::hidden('banners_id',  '', array('class'=>'form-control', 'id'=>'banners_id')); ?>

		  <div class="modal-body">						
			  <p>Are you sure you want to delete this banner?</p>
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
		  </div>
		  <?php echo Form::close(); ?>

		</div>
	  </div>
	</div>
    	 
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>