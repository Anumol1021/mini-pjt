<?php $__env->startSection('content'); ?>
<section class="content">
    <!-- Default box -->
  <div class="box">
    <div class="box-body">
      <h2>Brands</h2>
         <!-- /.box-header -->
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
           <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <!-- /.box-header -->
                    <!-- form start -->                        
                    <div class="box-body">
                      <?php if( count($errors) > 0): ?>
                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        <?php echo e($error); ?>

                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      <?php endif; ?>
                       <?php echo Form::open(array('url' =>'admin/addNewBrands', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')); ?>

                     
                    <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">URL</label>
                        <div class="col-sm-10 col-md-4">
                         <input type="text" name="url" class="form-control">
                          <span class="help-block hidden"><?php echo e(trans('labels.textRequiredFieldMessage')); ?></span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Services</label>
                        <div class="col-sm-10 col-md-4">
                          <select class="form-control" name="servicename" required="required">
                            <option value="">Select </option>
                            <?php if(count($result['services'])>0): ?>
                            <?php $__currentLoopData = $result['services']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $service): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($service->slug); ?>"><?php echo e($service->slug); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 
                              <?php endif; ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Image')); ?></label>
                        <div class="col-sm-10 col-md-4">
                         <?php echo Form::file('newImage', array('id'=>'newImage')); ?>

                         <br>
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
                        <div class="col-sm-10 col-md-4">
                          <select class="form-control" name="status">
                            <option value="Active"><?php echo e(trans('labels.Active')); ?></option>
                            <option value="InActive"><?php echo e(trans('labels.InActive')); ?></option>
                          </select>
                        </div>
                      </div>
                     <!-- /.box-body -->
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.SubmitNews')); ?></button>
        								<a href="<?php echo e(URL::to('admin/listingBrands')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
        							</div>
                       <!-- /.box-footer -->
                       <?php echo Form::close(); ?>

                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body --> 
            </div>
            <!-- /.box --> 
          </div>
           <!-- /.col --> 
        </div>
        <!-- /.row --> 
      </div>
        <!-- Main row --> 
    </div> 
	</div>
    <!-- /.row --> 
</section>
  <!-- /.content --> 
<script src="<?php echo asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js'); ?>"></script>
<script type="text/javascript">
		$(function () {
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>