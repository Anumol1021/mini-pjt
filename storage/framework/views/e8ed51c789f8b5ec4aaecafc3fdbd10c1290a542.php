<?php $__env->startSection('content'); ?>
<section class="abt-tp">
	<div class="container">
		<div class="col-md-12">
			<h3>Search "<?php echo e(request()->input('q')); ?>"</h3>
			<ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
        <li class="breadcrumb-item active">Search</li>
     </ul>
		</div>
	</div>
</section>
<div class="container">
  <ul class="clearfix">
      <?php if($search_result): ?>
      <?php $__currentLoopData = $search_result; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $search): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li data-aos="fade-up">
      <h5><u><a href="<?php if($search->slug): ?><?php echo e(route('comdetails',$search->slug)); ?><?php endif; ?>"><?php echo strip_tags($search->title); ?></a></u></h5>
      <p><?php echo strip_tags($search->description); ?></p>
    </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      <?php endif; ?>
  </ul>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>