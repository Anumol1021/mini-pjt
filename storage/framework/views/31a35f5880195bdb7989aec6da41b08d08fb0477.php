<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="col-lg-2">
				<div class="footer_intro">
					<div class="logo footer_logo">
						<a href="index.html"><img src="<?php echo asset('resources/assets/front'); ?>/images/logo.png" class="img-fluid"></a>
						<p>Security Systems</p>
					</div>
					<div class="footer_social">
						<ul>
							<li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
							<li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
			   <div class="row">
				  	<div class="col-lg-6">
				   	 	<div class="footer_col">
							<div class="footer_col_title">Products</div>
							<ul>
							 <?php
							 $service = DB::table('service')->where('status', '=', 'Active')->get();
							 ?>
							 <?php if(!$service->isEmpty()): ?>
							 <?php $__currentLoopData = $service; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $services): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<li>
									<a href="<?php echo e(route('servicesdetails',$services->slug)); ?>" ><?php echo ucfirst ($services->name); ?></a>
								</li>
							 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							 <?php endif; ?>	
							</ul>
						</div>
				   	</div>
				   	<div class="col-lg-6">
				   	 	<div class="footer_col">
							<div class="footer_col_title">Menu</div>
							<ul>
								<li><a href="<?php echo e(route('home')); ?>">Home</a></li>
								<li><a href="<?php echo e(route('aboutus')); ?>">About us</a></li>
								<li><a href="<?php echo e(route('solution')); ?>">Solution</a></li>
								<li><a href="<?php echo e(route('services')); ?>">Services</a></li>
								<li><a href="<?php echo e(route('contactus.index')); ?>">Contact</a></li>
							</ul>
						</div>
				   	</div>
				</div>	
			</div>
			<div class="col-lg-3">
				<div class="footer_col">
					<div class="footer_col_title">Quick Contact</div>
					<ul>
						<li>Building No. 79, Ali Bin Abi Taleb St. 820, Zone No. 40, Al Asiri, Doha, Qatar</li>
						<li><strong>P:</strong> <a href="tel:+974 44583926">+974 44583926</a></li>
						<li> <strong>F:</strong> +974 44621890</li>
						<li> <strong>E:</strong> info@alsaqrsecurity.com</li>
					</ul>
				</div>
			</div>
			<div class="col-lg-2 text-right">
				<img src="<?php echo asset('resources/assets/front'); ?>/images/moi.png" class="img-fluid" style="width: 110px">
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="footer_cr_2">2017 All rights reserved</div>
			</div>
		</div>
	</div>
</footer>
	<section class="ftr-btm">
		<div class="container">
			<div class="col-md-12">
				© 2019 Al Saqr. All Rights Reserved.
			</div>
		</div>
	</section>
</div>