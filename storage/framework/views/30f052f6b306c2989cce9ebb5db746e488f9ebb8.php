<?php $__env->startSection('content'); ?>
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Add Banner</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        <?php if(count($errors) > 0): ?>
                              <?php if($errors->any()): ?>
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <?php echo e($errors->first()); ?>

                                </div>
                              <?php endif; ?>
                          <?php endif; ?>
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
						
										 <div class="box-body">
										 
											<?php echo Form::open(array('url' =>'admin/updateBanner', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')); ?>

											  
												<?php echo Form::hidden('id',  $result['banners'][0]->banners_id , array('class'=>'form-control', 'id'=>'id')); ?>

												<?php echo Form::hidden('oldImage',  $result['banners'][0]->banners_image, array('id'=>'oldImage')); ?>

												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Title')); ?> </label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('banners_title', $result['banners'][0]->banners_title, array('class'=>'form-control','id'=>'banners_title')); ?>

												  </div>
												</div>
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Description</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('banners_desc', $result['banners'][0]->banners_desc, array('class'=>'form-control ','id'=>'banners_desc')); ?>

												  </div>
												</div>
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Link Text</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('banners_link_text', $result['banners'][0]->banners_link_text, array('class'=>'form-control','id'=>'banners_link_text')); ?>

												  </div>
												</div>
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Link</label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::text('banners_link', $result['banners'][0]->banners_link, array('class'=>'form-control','id'=>'banners_link')); ?>

												  </div>
												</div>
											  
											  
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Image')); ?></label>
												  <div class="col-sm-10 col-md-4">
													<?php echo Form::file('newImage', array('id'=>'banners')); ?>

													<br>
											
													<img src="<?php echo e(asset('').$result['banners'][0]->banners_image); ?>" alt="" width=" 100px">
												  </div>
												</div>
												
												
												
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label"><?php echo e(trans('labels.Status')); ?></label>
												  <div class="col-sm-10 col-md-4">
													  <select class="form-control" name="status">
														  <option value="1" <?php if($result['banners'][0]->status==1): ?> selected <?php endif; ?>><?php echo e(trans('labels.Active')); ?></option>
														  <option value="0" <?php if($result['banners'][0]->status==0): ?> selected <?php endif; ?>><?php echo e(trans('labels.Inactive')); ?></option>
													  </select>
													  
												  </div>
												</div>
												
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary"><?php echo e(trans('labels.Update')); ?></button>
												<a href="<?php echo e(URL::to('admin/listingBanners')); ?>" type="button" class="btn btn-default"><?php echo e(trans('labels.back')); ?></a>
											  </div>
											  <!-- /.box-footer -->
											<?php echo Form::close(); ?>

										</div>
									</div>
								
						
						
						
						
						
						
						
						
						
						
						
						
						
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 
<?php $__env->stopSection(); ?> 
<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>