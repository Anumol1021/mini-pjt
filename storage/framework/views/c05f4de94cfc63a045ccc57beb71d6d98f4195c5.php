<?php $__env->startSection('content'); ?>
<section class="abt-tp">
	<div class="container">
		<div class="col-md-12">
			<h3>Board of Directors</h3>
			<ul class="breadcrumb">
 			 	<li class="breadcrumb-item"><a href="<?php echo e(route('home')); ?>">Home</a></li>
 				<li class="breadcrumb-item active">Board of Directors</li>
			</ul>
		</div>
	</div>
</section>

<section class="directors clearfix">
	<div class="container">
		<div class="col-md-12">
		   	<ul class="gridder">
		   		<?php if(!$board->isEmpty()): ?>
		   		<?php $__currentLoopData = $board; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $boardof): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="gridder-list" data-aos="fade-up">
                   <figure>
	                   	<img src="<?php echo asset($boardof->image); ?>"> 
	                   	<figcaption><?php echo e($boardof->name); ?> <br> 
	                   		<span> <?php echo e($boardof->position); ?></span> 
	                   		<h5>KNOW MORE</h5>
	                   	</figcaption>
	                   	<div class="scroll-down">
	                   		<div class="scroll-down__line"></div>
	                   	</div>
                   </figure>
                </li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
            </ul>
		</div>
	</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.front.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>