<?php if(!$banners->isEmpty()): ?>
<section class="slider">
	<div class="tp-banner-container">
		<div id="slider2" class="tp-banner" >
			<ul>
			  <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li data-transition="fade" data-slotamount="5" data-masterspeed="700" > <img src="<?php echo e(asset("$banner->banners_image")); ?>" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
					<div class="tp-caption skewfromleft large_text customout"
						data-x="0"
						data-y="center" data-voffset="-45"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="800"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 6"><?php echo $banner->banners_title; ?>

					</div>
					<div class="tp-caption medium_dark skewfromleft customout"
						data-x="0"
						data-y="center" data-voffset="45"
						data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
						data-speed="800"
						data-start="1100"
						data-easing="Power4.easeOut"
						data-endspeed="300"
						data-endeasing="Power1.easeIn"
						data-captionhidden="on"
						style="z-index: 9"><?php echo $banner->banners_desc; ?>

					</div>
				</li>
			  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
    </div>
</section>
<?php endif; ?>  