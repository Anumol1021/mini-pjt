<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo e(asset('resources/views/admin/images/admin_profile/1499174950.avatar5.png')); ?>" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?php echo e($user->name); ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
				<li class="header">HOME</li>
				<li class="treeview <?php if(request()->segment(2) == 'listingCoupons' || request()->segment(2) == 'addCoupons'|| request()->segment(2) == 'editCoupons'): ?> active <?php endif; ?>">
					<a href="<?php echo e(route('admin.dashboard', ['reportBase' => 'this_month'])); ?>"> <i class="fa fa-home"></i><span> Dashboard</span></a>
				</li>
				<!--<li class="treeview <?php if(request()->segment(2) == 'listingCoupons' || request()->segment(2) == 'addCoupons'|| request()->segment(2) == 'editCoupons'): ?> active <?php endif; ?>">
					<a href="<?php echo e(route('admin.pages')); ?>"> <i class="fa fa-file"></i><span> Pages</span></a>
				</li>-->
            	<li class=" <?php if(request()->segment(2) == 'listingBanners'|| request()->segment(2) == 'editBanner' || request()->segment(2) == 'addBanner'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingBanners')); ?>">
                    <i class="fa  fa-picture-o"></i> <span>Banners</span>
                </a>
                
            </li>
	
            <li class=" <?php if(request()->segment(2) == 'listingDirectors'|| request()->segment(2) == 'editDirectors' || request()->segment(2) == 'addDirectors'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingDirectors')); ?>">
                    <i class="fa fa-user"></i> <span>Board of Directors</span>
                </a>
            </li>
			
            <li class=" <?php if(request()->segment(2) == 'listingTestimonials'|| request()->segment(2) == 'editTestimonials' || request()->segment(2) == 'addTestimonials'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingTestimonials')); ?>">
                    <i class="fa fa-circle-o"></i> <span>Testimonials</span>
                </a>
                
            </li>
         
            <li class=" <?php if(request()->segment(2) == 'listingCompanies'|| request()->segment(2) == 'editCompanies' || request()->segment(2) == 'addCompanies'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingCompanies')); ?>">
                    <i class="fa fa-bookmark"></i> <span>Companies</span>
                </a>
                
            </li>


       <!--     <li class=" <?php if(request()->segment(2) == 'listingProducts'|| request()->segment(2) == 'editProducts' || request()->segment(2) == 'addProducts'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingProducts')); ?>">
                    <i class="fa fa-bookmark"></i> <span>Products</span>
                </a>
                
            </li>-->


            <li class=" <?php if(request()->segment(2) == 'listingNews'|| request()->segment(2) == 'editNews' || request()->segment(2) == 'addCompanies'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingNews')); ?>">
                    <i class="fa fa-newspaper-o"></i> <span>News & Events</span>
                </a>
                
            </li>

             <li class=" <?php if(request()->segment(2) == 'listingSubscribers'|| request()->segment(2) == 'editSubscribers' || request()->segment(2) == 'addSubscribers'): ?> active <?php endif; ?>">
                <a href="<?php echo e(route('admin.listingSubscribers')); ?>">
                    <i class="fa fa-user"></i> <span>Subscribers</span>
                </a>
                
            </li>
    
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->