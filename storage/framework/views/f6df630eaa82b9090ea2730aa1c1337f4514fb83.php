

<section class="qpih clearfix">
	<div class="container">
		<div class="qpih-sec clearfix">
			<ul>
				<li><div class="qpih-img"><img src="<?php echo asset('resources/assets/front'); ?>/images/Quality.gif"></div><h5>QUALITY</h5></li>
				<li><div class="qpih-img"><img src="<?php echo asset('resources/assets/front'); ?>/images/Performance.gif"></div><h5>PERFORMANCE</h5></li>
				<li><div class="qpih-img"><img src="<?php echo asset('resources/assets/front'); ?>/images/Integrity.gif"></div><h5>INTEGRITY</h5></li>
				<li><div class="qpih-img"><img src="<?php echo asset('resources/assets/front'); ?>/images/Health.gif"></div><h5>HEALTH AND SAFETY</h5></li>
			</ul>
		</div>
	</div>
</section>


<footer>
	<div class="container">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-7">
					<div class="nwsltr">
						<h3><span>Join Our Newsletter</span><br>Subscribe to Our Newsletter</h3>
						<div id="sucdiv" class="success"></div>
						<span id="errdiv" ></span>
						<div class="form-group">
							<form method="post" action="<?php echo e(route('subscribe')); ?>" class="clearfix">
							<?php echo e(csrf_field()); ?>

			                <input type="email" class="form-control" id="email_input" placeholder="Enter Your Email address" name="email_address"  data-rule-required="true" data-rule-email="true" data-msg-email="Please enter a valid email address">
			                <button class="btn-nws sub" type="button" >Enter</button>
							
			             	</form>
						</div>
					</div>
				</div>
				<div class="col-md-5">
					<div class="ftr-con"><a href="<?php echo e(route('contactus.index')); ?>">
						<h3><span>Ask a question</span><br>Contact us</h3>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</footer>


<section class="ftr-btm clearfix">
	
	<div class="container clearfix">
		<div class="col-md-12 clearfix">
			<div class="social-left">
				<ul>
					<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					<li><a href="#"><i class="fa fa-instagram"></i></a></li>
				</ul>
			</div>
			<div class="ftr-nav">
				<ul>
					<li><a href="<?php echo e(route('careers')); ?>">Careers</a></li>
					<li><a href="<?php echo e(route('company')); ?>">Companies</a></li>
					<li><a href="<?php echo e(route('aboutus')); ?>">About</a></li>
					<li><a href="<?php echo e(route('news')); ?>">News & Events </a></li>
					<li><a href="<?php echo e(route('contactus.index')); ?>">Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class="col-md-12 clearfix">
			<div class="ftr-lft">Copyright© 2019-2020 AL RABBAN All Rights Reserved.</div>
			<div class="ftr-rgt">Powered by  <a href="https://www.whytecreations.com/" target="_blank" title="Webdesign companies in qatar"> whytecreations </a></div>
		</div>
	</div>
	
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.18.0/jquery.validate.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/notify/0.4.2/notify.js" type="text/javascript"></script>

	<script>
$(document).ready(function(){
	$('#email_input').keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  $(".sub").click();
		  return false;
		}
	 });
	$(".sub").click(function(){
		event.preventDefault();
		var id = $(this).data('id');
		var frm = $(this).parent();
		var post_url = frm.attr("action"); //get form action url
		var request_method = frm.attr("method"); //get form GET/POST method
		var form_data = frm.serialize(); //Encode form elements for submission
		var fl = frm.valid({
			errorElement : '#errdiv',
			errorLabelContainer: '.errorTxt'
		});
		if(fl){
			$.ajax({
				url : post_url,
				type: request_method,
				data : form_data
			}).done(function(response){ 
				var type = (response.res_code == '1')? 'success' : 'danger';
				frm[0].reset();
				if(response.res_code == '1')
			 	    $('#sucdiv').html(response.res_msg)
				else
				    $('#errdiv').html(response.res_msg)
			})
		}
	});
	
});
</script>	