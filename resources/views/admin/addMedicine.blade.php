@extends('layouts.admin.app')
@section('content')
<section class="content">
  <div class="box">
    <div class="box-body">
      <h2>Add Review</h2>
      <div class="box-body">
		    <div class="row">
          <div class="col-md-12">
            <div class="box">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box-body">
                      @if( count($errors) > 0)
                      @foreach($errors->all() as $error)
                      <div class="alert alert-success" role="alert">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                        {{ $error }}
                      </div>
                      @endforeach
                      @endif
                       {!! Form::open(array('url' =>'admin/addNewMedicine', 'method'=>'post', 'class' => 'form-horizontal form-validate', 'enctype'=>'multipart/form-data')) !!}
                      
							        <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Description Id</label>
                        <div class="col-sm-10 col-md-4">
						
							<select class="form-control" name="desid" id="desid">
												<option value="">Select </option>
												@if($dsriptn->count() > 0)
													@foreach($dsriptn as $row)
														<option value="{{$row->id}}">{{$row->id}}</option>
													@endforeach	
												@endif
											
											</select>
						
                         
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Medicine</label>
                        <div class="col-sm-10 col-md-4">
                         <input type="text" name="medicine" class="form-control field-validate">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Medicine Description</label>
                        <div class="col-sm-10 col-md-8">
                          <textarea id="editor" name="medicinedesc" class="form-control" rows="10" cols="80"></textarea> <br>
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="name" class="col-sm-2 col-md-3 control-label">Time</label>
                        <div class="col-sm-10 col-md-4">
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Morning"></td>
                            <td>Morning</td>
                          </tr>
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Afternoon"></td>
                            <td>Afternoon</td>
                          </tr>
                          <tr> 
                            <td><input type="checkbox" name="time[]" class="mode1 mode_checkbox_mode" data-id="" value="Night"></td>
                            <td>Night</td>
                          </tr>
                        </div>
                      </div>

                    
        							<div class="box-footer text-center">
        								<button type="submit" class="btn btn-primary">{{ trans('labels.SubmitNews') }}</button>
        								<a href="{{ URL::to('admin/listingMedicine')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
        							</div>
                       {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
	</div>
</section>
<script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>
<script type="text/javascript">
		$(function () {
			$("textarea").summernote({height: "400px",});
    });
</script>
@endsection 