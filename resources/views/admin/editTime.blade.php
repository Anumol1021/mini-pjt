@extends('layouts.admin.app')
@section('content')
 <section class="content">
   
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Edit Details</h2>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-12">
              		<div class="box box-info">
                    <br>                       
                        @if (count($errors) > 0)
                              @if($errors->any())
                                <div class="alert alert-success alert-dismissible" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{$errors->first()}}
                                </div>
                              @endif
                          @endif
                        <!--<div class="box-header with-border">
                          <h3 class="box-title">Edit category</h3>
                        </div>-->
                        <!-- /.box-header -->
                        <!-- form start -->  
						<div class="panel with-nav-tabs  panel-default">
							<!--<div class="panel-heading" style="border:none;">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#English" data-toggle="tab">English</a></li>
										<li><a href="#Arabic" data-toggle="tab">Arabic</a></li>
									</ul>
							</div>-->
							<div class="panel-body">
								<div class="tab-content">
									<div class="tab-pane fade in active" id="English">
						
										 <div class="box-body">
										 
											{!! Form::open(array('url' =>'admin/updateTime', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
											  
											{!! Form::hidden('id',  $result['time'][0]->id , array('class'=>'form-control', 'id'=>'id')) !!}

												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Time</label>
												  <div class="col-sm-10 col-md-4">
													{!! Form::text('time', $result['time'][0]->time, array('class'=>'form-control','id'=>'time')) !!}
												  </div>
												</div>
												
												<div class="form-group">
												  <label for="name" class="col-sm-2 col-md-3 control-label">Date</label>
												  <div class="col-sm-10 col-md-4">
													{!! Form::text('datee', $result['time'][0]->datee, array('class'=>'form-control','id'=>'datee')) !!}
												  </div>
												</div>

											  
												
											  <!-- /.box-body -->
											  <div class="box-footer text-center">
												<button type="submit" class="btn btn-primary">{{ trans('labels.Update') }}</button>
												<a href="{{ URL::to('admin/listingTime')}}" type="button" class="btn btn-default">{{ trans('labels.back') }}</a>
											  </div>
											  <!-- /.box-footer -->
											{!! Form::close() !!}
										</div>
									</div>
							
						
                  </div>
              </div>
            </div>
            
          </div>
         <!-- /.box-body --> 
        </div>
        <!-- /.box --> 
      </div>
   
  </section>
  <!-- /.content --> 

   <script src="{!! asset('resources/views/admin/plugins/jQuery/jQuery-2.2.0.min.js') !!}"></script>


<script type="text/javascript">
		$(function () {
			
			//bootstrap WYSIHTML5 - text editor
			//
			$("textarea").summernote({height: "400px",});
			//$("textarea").wysihtml5();
			
    });
</script>
@endsection 