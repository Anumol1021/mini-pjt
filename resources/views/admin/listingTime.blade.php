@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Time</h2>
            <div class="row">
              <div class="col-xs-12">
                <div class="btn-group pull-right" >
                  <a class="btn btn-sm btn-primary" href="{{ route('admin.addTime') }}"><i class="fa fa-plus"></i> Add New</a>
                </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Time</th>
                      <th>Date</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['time'])>0)
                    @foreach ($result['time'] as $key=>$time)
                        <tr>
                            <td>{{ $time->id }}</td>
                            <td>{{ $time->time }}</td>
                            <td>{{ $time->datee }}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editTime/{{ $time->id }}" class="badge bg-light-blue">Edit</a> 
                        </tr>
                    @endforeach
                    @else
                       <tr>
                          <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['time']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  



@endsection