/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	//config.extraPlugins =  'imageuploader,imagebrowser,colorbutton,panelbutton';
	config.extraPlugins =  'imageuploader,imagebrowser';
	config.filebrowserImageUploadUrl =  base_url+"/vendor/ckeditor/plugins/imageuploader/imgupload.php?base_url="+base_url;
	config.filebrowserBrowseUrl =  base_url+"/vendor/ckeditor/plugins/imageuploader/imgbrowser.php?base_url="+base_url;
	config.toolbar = 'MyToolbar';
 	config.enterMode = CKEDITOR.ENTER_BR;
	
	config.toolbar_MyToolbar =
	[
		{ name: 'document', items : [ 'NewPage','Preview'  ] },
		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
		{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
		{ name: 'insert', items : [ 'Table','HorizontalRule','Smiley','SpecialChar','PageBreak'
                 ,'Iframe' ] },
                '/',
		{ name: 'styles', items : [ 'Styles','Format','Font' ,'FontSize'] },
		{ name: 'colors', items: [ 'TextColor', 'BGColor' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','CreateDiv',
	'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl'  ] },
		{ name: 'links', items : [ 'Link','Unlink','Anchor','Image'] },
		{ name: 'tools', items : [ 'Maximize','-','About' ] }
	];
};
