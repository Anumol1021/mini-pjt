@if(!$subscribers->isEmpty())
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Email</td>
            <td>Created Date</td>
        </tr>
        </thead>
        <tbody>
        @foreach ($subscribers as $subscriber)
            <tr>
                <td>{{ $subscriber->subscriber_id }}</td>
                <td>
                  {{ $subscriber->email_address }}
                </td>
                <td> {{ $subscriber->created_date }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif