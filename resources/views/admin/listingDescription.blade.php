@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Descriptions</h2>
            <div class="row">
              <div class="col-xs-12">
         <div class="btn-group pull-right" >
                        <a class="btn btn-sm btn-primary" href="{{ route('admin.addDescription') }}"><i class="fa fa-plus"></i> Add New</a>
                 </div>
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Register Id</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['description'])>0)
                    @foreach ($result['description'] as $key=>$description)
                        <tr>
                            <td>{{ $description->id }}</td>
                            <td>{{ $description->regid }}</td>
                            <td>{!! substr($description->description,0,200 )!!}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editDescription/{{ $description->id }}" class="badge bg-light-blue">Edit</a> 
                            
                           
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['description']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  



@endsection