@extends('layouts.admin.app')
@section('content')
<!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
    <div class="box">
        <div class="box-body">
            <h2>Pages</h2>
            <div class="row">
              <div class="col-xs-12">
   
                <table class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Type</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  @if(count($result['pages_description'])>0)
                    @foreach ($result['pages_description'] as $key=>$pages_description)
                        <tr>
                            <td>{{ $pages_description->id }}</td>
                            <td>{{ $pages_description->name }}</td>
                            <td>{{ $pages_description->type }}</td>
                            <td>{{ $pages_description->status }}</td>
                            <td><a data-toggle="tooltip" data-placement="bottom" title="Edit" href="editPage/{{ $pages_description->id }}" class="badge bg-light-blue"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> 
                            
                        
                        </tr>
                    @endforeach
                    @else
                       <tr>
                            <td colspan="5">NoRecordFound</td>
                       </tr>
                    @endif
                  </tbody>
                </table>
                <div class="col-xs-12 text-right">
                  {{$result['pages_description']->links()}}
                </div>
              </div>
            </div>
          </div>
     </section>  

    <!-- deleteBannerModal -->
  <!--<div class="modal fade" id="deletePageModal" tabindex="-1" role="dialog" aria-labelledby="deletePageModalLabel">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="deletePageModalLabel">Delete Page</h4>
      </div>
      {!! Form::open(array('url' =>'admin/deleteCompanies', 'name'=>'deleteCompanies', 'id'=>'deleteCompanies', 'method'=>'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data')) !!}
          {!! Form::hidden('action',  'delete', array('class'=>'form-control')) !!}
          {!! Form::hidden('id',  '', array('class'=>'form-control', 'id'=>'id')) !!}
      <div class="modal-body">            
        <p>Are you sure you want to delete this Company?</p>
      </div>
      <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      <button type="submit" class="btn btn-primary" id="deleteBanner">Delete</button>
      </div>
      {!! Form::close() !!}
    </div>
    </div>
  </div>-->
       
@endsection 
@section('js')
<script>
$(document).on('click', '#deleteCompaniesId', function(){
var id = $(this).attr('Companyid');
$('#id').val(id);
$("#deleteCompaniesModal").modal('show');
});
</script> 
@endsection