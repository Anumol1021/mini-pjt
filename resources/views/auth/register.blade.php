@extends('layouts.front.app')

@section('content')
<div class="main">


<div class="breadcrumb lst-bread"> <a class="breadcrumb-item" href="{{ route('home') }}">Home</a> <span class="breadcrumb-item active">Registration</span> </div>

	<div class="login">
		<div class="row">
			<div class="col-md-6"><div class="lgn-lft"><img src="{!! asset('resources/assets/front/') !!}/images/login-lft.jpg" class="img-fluid"></div></div>
			<div class="col-md-6">
				<h4>Create an Account </h4>
				 <form  role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                    
					
					 <div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="First Name" required name="name" value="{{ old('name') }}" autofocus style="border-radius:5px 5px 0 0">
						@if ($errors->has('name'))
							<span class="help-block">
								<strong>{{ $errors->first('name') }}</strong>
							</span>
                        @endif
                    </div>
                    
					<div class="form-group clearfix">
						<input type="text" class="form-control" placeholder="Last Name" required name="lname" value="{{ old('lname') }}">
						@if ($errors->has('lname'))
							<span class="help-block">
								<strong>{{ $errors->first('lname') }}</strong>
							</span>
                        @endif
                    </div>
					<!--<div class="form-group clearfix"><i class="fa fa-envelope-o"></i><input type="text" class="form-control" placeholder="Email Address"></div>
					<div class="form-group clearfix"><i class="fa fa-key"></i><input type="password" class="form-control" placeholder="New Password"></div>
					<div class="form-group clearfix"><i class="fa fa-key"></i><input type="password" class="form-control" placeholder="Retype Password"></div>
					-->
					
					<div class="form-group clearfix">
					<input type="text" class="form-control" placeholder="Email Address" required name="email" value="{{ old('email') }}">
					@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
                        @endif
					</div>
					<div class="form-group clearfix">
					<input type="password" class="form-control" placeholder="New Password" required  name="password">
					</div>
					<div class="form-group clearfix">
					<input type="password" class="form-control" placeholder="Retype Password" required name="password_confirmation">
					</div>
					
					<!-- <div class="form-group clearfix">
					<input type="text" class="form-control" placeholder="Your Email" required name="email" value="{{ old('email') }}">
						@if ($errors->has('email'))
							<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
                        @endif
                    </div>
                    
                    <div class="form-group clearfix">
							<input type="password" class="form-control" placeholder="Password" required  name="password">
                    </div>
                    
                    <div class="form-group clearfix">
						<input type="password" class="form-control" placeholder="Confirm Password" required name="password_confirmation" style="border-radius: 0 0 5px 5px">
                    </div>
					
					-->
                    
                    <!--<button type="submit" class="log">Register</button>-->
					
					<div class="form-group clearfix"><button type="submit" class="btn-log">Create account</button></div>
					<p>Already you have account?  <a href="{{ route('login') }}"> Login </a></p>
				</form>
			</div>
		</div>
	</div>

</div>

@endsection
