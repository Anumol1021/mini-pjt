@extends('layouts.front.app')

@section('content')
<div class="breadcrumb lst-bread"> <a class="breadcrumb-item" href="{{ route('home') }}">Home</a> <span class="breadcrumb-item active">Forgot Password</span> </div>

	<div class="login">
		<div class="row">
			<div class="col-md-6"><div class="lgn-lft"><img src="{!! asset('resources/assets/front/') !!}/images/login-lft.jpg" class="img-fluid"></div></div>
			<div class="col-md-6">
				<h4>Forgot Your Password? <br> <span>Please enter your email address below. You will receive a link to reset your password.</span></h4>
				  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
				 <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
					{{ csrf_field() }}
					<div class="form-group clearfix"><i class="fa fa-envelope-o"></i>
					<!-- <input type="text" class="form-control" placeholder="Email Address"> -->
					<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email Address">
					</div>
					 @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
					<div class="form-group clearfix"><button class="btn-log">Submit</button></div>
					<p>Backto <a href="{{ route('login') }}"> Login </a></p>
				</form>
			</div>
		</div>
	</div>

</div>

@endsection
