@extends('layouts.front.app')

@section('content')

<div class="breadcrumb lst-bread"> <a class="breadcrumb-item" href="{{ route('home') }}">Home</a> <span class="breadcrumb-item active">Reset Password</span> </div>

	<div class="login">
		<div class="row">
			<div class="col-md-6"><div class="lgn-lft"><img src="{!! asset('resources/assets/front/') !!}/images/login-lft.jpg" class="img-fluid"></div></div>
			<div class="col-md-6">
				<h4>Forgot Your Password? <br> <span>Please enter your email address below. You will receive a link to reset your password.</span></h4>
				  @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
			 <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

					<input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                          
                                <input id="email" type="email" class="form-control" name="email" placeholder="Email Address" value="{{ $email or old('email') }}" required autofocus >

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                          
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                           
                                <input id="password" type="password" class="form-control" name="password" required placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                           
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                           
                          
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            
                        </div>
						
						
					<div class="form-group clearfix"><button class="btn-log"> Reset Password</button></div>
					<p>Backto <a href="{{ route('login') }}"> Login </a></p>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
